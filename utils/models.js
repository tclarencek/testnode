let mongoose = require('mongoose');
module.exports = {

    Auth: new mongoose.model('authenticationData', new mongoose.Schema({
        token: String,
        userId: String //uuid
    })),

    User: new mongoose.model('users', new mongoose.Schema({
        name: String,
        email: String,
        password: String
    })),

    Post: new mongoose.model('posts', new mongoose.Schema({
        title: String,
        excerpt: String,
        content: String,
        created_at: Date,
        updated_at: Date,
        postImage: String,
        userId: String,
        categoryId:String
    })),
    Comment:new mongoose.model('comments', new mongoose.Schema({
        comment:String, //content
        userId:String,
        created_at:Date,
        updated_at:Date,
        postId:String
    }))
};