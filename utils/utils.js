module.exports = {
    db_connection: function () {

    },
    is_prod_env: function () {

    },
    err_response: function (res,code, data) {
        // res.header("Access-Control-Allow-Origin", "*");
        // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        console.log(data)
        res.status(code).json( {
            status: "err",
            data
        })
    },
    success_response: function (res,code, data) {
        // res.header("Access-Control-Allow-Origin", "*");
        // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.status(code).json( {
            status: "success",
            data
        })
    }
};

