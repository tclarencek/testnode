var express = require('express');
var router = express.Router();
let utils = require('../utils/utils');
let config = require('../utils/config');
let jwt = require('jsonwebtoken');
let {Post, User,Comment} = require('../utils/models');
/* GET home page. */
router.get('/:id/posts', function(req, res, next) {
    Post.find({categoryId:req.params.id},function(err,comments){
      if(err){
        utils.err_response(res,400,"Couldn't fetch comments");
        throw err;
      }else{
        utils.success_response(res,200,comments);
      }
    });
});

module.exports = router;
