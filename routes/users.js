var express = require('express');
var router = express.Router();
let utils = require('../utils/utils');
let config = require('../utils/config');
let jwt = require('jsonwebtoken');
let {User} = require('../utils/models');
let uuid = require('uuid').v4;

/* GET users listing. */
router.get('/login', function(req, res, next) {
    if( req.query.email && req.query.password){
        let email =req.query.email;
        let password = req.query.password;
        User.find({
            email,password
        },function(err,users){
            if(err){
                utils.err_response(res,400,"Something went wrong err_code 4");
                throw new err;
            }else{
                if(users.length===1){
                    let _id = users[0]._id;
                    jwt.sign({email,password,_id}, config.jwtSecret, (err, token) => {
                        if (err) {
                            utils.err_response(res,400,"Something went wrong err_code 3");
                            console.error(err);
                        } else {
                            utils.success_response(res, 200, token);
                        }
                    })
                }else{
                    utils.err_response(res,400,"No such user");
                }

            }
        });
    }else{
        utils.err_response(res,400,"Insufficient parameters")
    }

});

router.get('/create',function(req,res,next){
    if( req.query.email && req.query.password){
        let email  = req.query.email;
        let password  = req.query.password;
        let name  = req.query.name;
        let user = new User({
            email,password
        });
        User.find({email},(err,users)=>{
           if(err) {
               utils.err_response(res,500,"Something went wrong. err_code:1");
               throw new err;
           }else{
               console.log(users);
               if(users.length===0){
                   user.save((err, user)=>{
                       if (err){
                           utils.err_response(res,500,"Something went wrong. err_code:2");
                           throw new err;
                       }else {
                           jwt.sign({email,password,_id:user._id}, config.jwtSecret, (err, token) => {
                               if (!err) {
                                   utils.success_response(res,200,token);
                                   throw new err;
                               } else {
                                   utils.err_response(res, 400, "Access token not generated");
                               }
                           })
                       }
                   });
               }else{
                   utils.err_response(res, 400, "User already exists with email address");
               }
           }

        });

    }else{
        utils.err_response(res,400,"Insufficient parameters")
    }
});

module.exports = router;
