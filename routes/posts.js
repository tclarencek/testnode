var express = require('express');
var router = express.Router();
let utils = require('../utils/utils');
let config = require('../utils/config');
let jwt = require('jsonwebtoken');
let {Post, User,Comment} = require('../utils/models');
let uuid = require('uuid').v4;
let multer =  require('multer')

/* GET users listing. */
router.get('/ping', function (req, res, next) {
    utils.success_response(res, 200, "pong");
});

router.post('/create', function (req, res, next) {
    console.log(req.body);
    if (req.body.title && req.body.excerpt && req.body.content && req.body.categoryId) {
        let date = new Date();
        // var storage =   multer.diskStorage({
        //     destination: function (req, file, callback) {
        //         callback(null, './uploads');
        //     },
        //     filename: function (req, file, callback) {
        //         callback(null, file.originalname);
        //     }
        // });
        // var upload = multer({ storage : storage}).single('image');



        // upload(req,res,function(err){
        //     if(err){
        //         utils.err_response(res,400,"Cant upload file");
        //         return;
        //     }else{
        //
        //     }
        //
        // });
        let post = new Post({
            title: req.body.title,
            excerpt: req.body.excerpt,
            content: req.body.content,
            created_at: date,
            updated_at: date,

            postImage: req.body.image,
            userId: req.user.email,
            categoryId: req.body.categoryId
        });
        post.save(function (err, posst) {
            if (err) {
                console.log(err);
                utils.err_response(res, 400, "Couldn't save the post")
            } else {
                utils.success_response(res, 200, posst);
            }
        });
    } else {
        utils.err_response(res, 400, "Make sure all the fields are present")
    }
});

router.get("/:id",function(req,res,next){
    if(req.params.id){
        Post.find({_id:req.params.id},function(err,post){
           if(err){
               utils.err_response(res,400,"Couldn't fetch post");
               throw err;
           } else{
               utils.success_response(res,200,post.length===0?{}:post[0]);
           }
        });
    }else{
        utils.err_response(res, 400, "Something weird")
    }
});

router.get("/all/list", function (req, res, next) {
    Post.find({},function (err, posts) {
        if (err) {
            utils.err_response(res, 500, "Error fetching posts");
            throw err;
        } else {
            utils.success_response(res, 200, posts);
        }
    });
});
router.get('/:id/comment/all', function (req, res, next) {
    if (req.params.id) {
        let date = new Date();
            Comment.find({postId:req.params.id},function(err, comments){
                if(err){
                    utils.err_response(res, 400, "Couldn't fetch comments");
                    throw err;
                }else{
                    utils.success_response(res,200,comments);
                }
            })

    } else {
        utils.err_response(res, 400, "Something weird")
    }
});
router.post('/:id/comment/create', function (req, res, next) {
    console.log(JSON.stringify(JSON.stringify(req.user)));
    if (req.params.id) {
       if(req.body.comment){
           let date = new Date();
           let comment = new Comment({
               comment:req.body.comment,
               userId:req.user.email,
               created_at:new Date(),
               updated_at:new Date(),
               postId:req.params.id
           });
        comment.save((err,comment)=>{
            if(err){
                utils.err_response(res, 400, "That didnt work");
                throw err;
            }else{
                utils.success_response(res,200,comment)
            }
        });
       }else{
           utils.err_response(res, 400, "Insufficient params to create")
       }
    } else {
        utils.err_response(res, 400, "Something weird")
    }
});

module.exports = router;
