let createError = require('http-errors');
let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');

let indexRouter = require('./routes/index');
let usersRouter = require('./routes/users');
let postsRouter = require('./routes/posts');
let categoriesRouter = require('./routes/categories');
let app = express();
let utils = require("./utils/utils");
let config = require("./utils/config");
let jwt = require("jsonwebtoken");
let mongoose = require("mongoose");


//connect to the db

mongoose.connect("mongodb://127.0.0.1:27017/", {useNewUrlParser: true});
let database = mongoose.connection;
database.on('error',console.error.bind(console,"Error in console: "));
database.on('open',function(){
    console.log("DB connection successful");
});


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(function(req,res,next){

    res.header("Access-Control-Allow-Origin", "http://localhost:3000");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    next();
});
app.use(function(req,res,next){
    if(req.method==="OPTIONS"){
        next()
        return
    }

    let s = req.url.split("?")[0];
    let openRoutes = ['/user/login','/user/create','/feed'];
    // case its an unprotected route
    if(openRoutes.includes(s)){
        next();
        return;
    }
    // protected routes
    let headers =  req.headers['authorization'];
    if(!headers){
        utils.err_response(res, 403, "No Auth header");
    }else{
        headers = headers.split(" ");
        if(!headers[1]){
            utils.err_response(res, 403, "Access token absent");
        }else {
            let token = headers[1];
            jwt.verify(token,config.jwtSecret,function(err,AuthData){
                if(err){
                    // console.log(token);
                    utils.err_response(res, 403, "Access token invalid");
                }else{
                    req.user = AuthData;
                    console.log(AuthData);
                    next();
                }
            });
        }
    }

    console.log(req.url)
});

app.use('/', indexRouter);
app.use('/user', usersRouter);
app.use('/post',postsRouter);
app.use('/category',categoriesRouter);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
